#!/bin/bash

# Colors
NC='\033[0m' # No Color
#BLUE='\033[1;34m'
CYAN='\033[0;36m'
LCYAN='\033[1;36m'
#GREEN='\033[1;32m'
#PURP='\033[1;35m'
RED='\033[0;31m'
REDB='\033[1;31m'
YLW='\033[1;33m'
WHT='\033[1;37m'

q_and_a() {
if [ "$MCHOICE" != "2" ]; then
    echo -e '\n'${YLW}'>'${NC} General config
    read -p "$(echo -e "\nYour bittorrent client's watch directory (relative paths okay): \n\b ")" GWATCH
    
    while true
    do
        read -p "$(echo -e "\nRetrieve upload's metadata (JSON) (y/n)? ")" GMETADATA
        case $GMETADATA in
            [yY]* ) GMETADATA=true
                    read -p "$(echo -e "\nDirectory where your bittorrent client downloads the files: \n\b ")" GDOWNLOAD
                    break;;
            [nN]* ) GMETADATA=false
                    break;;
                * ) ;;
        esac
    done
    clearp
fi

echo -e '\n'${YLW}'>'${NC} Trackers config
read -p "$(echo -e "\nRED username: ")" TUSER
read -p "$(echo -e "\nRED password: ")" TPASS
clearp

if [ "$MCHOICE" != "2" ]; then
    echo -e '\n'${YLW}'>'${NC} Autosnatch config
    read -p "$(echo -e "\nIRC Key: ")" ASIRCKEY
    read -p "$(echo -e "\nAutosnatch Bot name: ")" ASBOTNAME
    read -p "$(echo -e "\nNickServ password for your Autosnatch Bot: ")" ASNICKPASS
    clearp
    read -p "$(echo -e "\n${WHT}Blacklist${NC} the following uploaders (e.g. blacklistuser1,blacklistuser2,blacklistuser3) (no spaces between commas)\n(do not enter your username)\n\nList usernames below (press [ENTER] if none): \n\b ")" BLACKLIST
    clearp
    echo -e '\n'${YLW}'>'${NC} Stats config
    read -p "$(echo -e "\nMaximum buffer decline over an hour before initializing autoshutdown of Varroa (in MB): ")" STBUFFERDEC
    read -p "$(echo -e "\nMinimum acceptable ratio before autosnatching is disabled (hit [ENTER] for default: 0.60): ")" STMINRATIO
        if [ -z "$STMINRATIO" ]; then
            STMINRATIO="0.6"
        fi
    read -p "$(echo -e "\nTarget ratio for calculating buffer (hit [ENTER] for default: 1.0): ")" STTARRATIO
        if [ -z "$STTARRATIO" ]; then
            STTARRATIO="1.0"
        fi
    
    clearp
fi
echo -e '\n'${YLW}'>'${NC} Web Server config
while true; do
    read -p "$(echo -e "\nDo you wish to have an embedded webserver serve your stats and/or allow remote snatching, y/n? ")" WSCHOICE
        case $WSCHOICE in
            [yY]* )
                    echo -e "\nNote: Your username and pass below should be unique to those previously entered for the tracker."
                    read -p "$(echo -e "\nSet username: ")" WSUSER
                    read -p "$(echo -e "\nSet password: ")" WSPASS
                    read -p "$(echo -e "\nSet a token name that remote sources will use when requesting stats and/or sending torrents: ")" WSTOKEN
                    read -p "$(echo -e "\nEnter a unique HTTPS port #: ")" WSPORT
                    read -p "$(echo -e "\nEnter the hostname of the machine hosting the server (hostname command): ")" WSHOST
                    echo -e "\n$(for each in $(seq 1 25); do printf "-"; done)\nNOTE 2: Varroa will try to generate a self-signed certificate on first launch automatically (requires openssl).\nOnce the HTTPS server is launched, you will need to force your browser to accept its certificate otherwise it will reject it and refuse to connect.\n\nURL of HTTPs webserver: https://$WSHOST:$WSPORT\n\nLogin: $WSUSER\nPassword: $WSPASS\n\nIf the above doesn't work, there may be additional steps needed.\nPlease consult the wiki (webserver > requirements): https://redacted.ch/wiki.php?action=article&id=224"
                    break;;
            [nN]* )
                    break;;
            * )
                echo -e "\nPlease answer the question.\n";;
        esac
done

clearp

echo -e '\n'${YLW}'>'${NC} Gitlab Page config
while true
do
    read -p "$(echo -e "\nDo you wish to host your stats on your gitlab page? \n(This requires a free Gitlab account dedicated to host said stats and requires git to be installed locally)\n\nAnswer (y/n): ")" GITLCHOICE
    case $GITLCHOICE in
        [yY]* )
                read -p "$(echo -e "\nHTTPs link for your gitlab repository: ")" GITHTTPS
                read -p "$(echo -e "\nGitlab username: ")" GITUSER
                read -p "$(echo -e "\nGitlab password: ")" GITPW
                break;;
        [nN]* )
                break;;
        * )
            echo -e "\nPlease answer the question.\n";;
    esac
done

clearp

echo -e '\n'${YLW}'>'${NC} Notifications config
while true
do
    read -p "$(echo -e "\nIf you have a pushover.net account, do you wish to send notifications when specific events occur (stat updates, torrent snatches, autosnatch disabling, etc?\n\nAnswer (y/n): ")" NTFYCHOICE
    case $NTFYCHOICE in
        [yY]* )
                read -p "$(echo -e "\nPushover UserID: ")" NTFYUSER
                read -p "$(echo -e "\nToken provided by pushover after registering an application with pushover (API Key): ")" NTFYTKN
                echo -e "\nWebhook section: Webserver to receive POST requests"
                read -p "$(echo -e "\nThe ${WHT}http_address:port${NC} for your receiving server: ")" NTFYADDRS
                read -p "$(echo -e "\nA secret token of your choosing, for verification purposes on the receiving side: ")" NTFYWHTKN
                break;;
        [nN]* )
                break;;
        * )
            echo -e "\nPlease answer the question.\n";;
    esac
done

clearp

if [ "$MCHOICE" != "2" ]; then
    echo -e '\n'${YLW}'>'${NC} Library config
    while true; do
        read -p "$(echo -e "\nVarroa's downloads subcommands allow to scan and sort your already downloaded releases.  You can export what you want to your music library folder, and varroa will suggest a new folder name based on collected metadata and your preferred pattern.\n\nThis will never remove files, nor will it interact with your bittorrent client to stop seeding or remove the torrent.\n\n(Note: An existing, writable library path must be provided. If using hard links, this folder must be on the same partition than the download_directory.)\n\nDo you wish to configure this (y/n)? ")" LIBCHOICE
        case $LIBCHOICE in
            [yY]* )
                    read -p "$(echo -e "\nLibrary path is the folder where releases will be exported, either as copies or hard links (relative path okay): ")" LIBDIR
                    read -p "$(echo -e "\nIf set to true, releases will be exported as hard links: they will use no extra space, but any modification will have an impact on files being seeded (default: false) (t/f): ")" LIBHL
                        if [ "$LIBHL" == "t" ]; then
                            LIBHL="true"
                        else
                            LIBHL="false"
                        fi
                    read -p "$(echo -e '\nOptional user template for folder naming, without subfolders.\nInformation about template formatting below:\n\n\$a: artists (if more than 3, "Various Artists")\n\$y: original year\n\$t: title\n\$s: source\n\$g: source with an additional indicator for CDs: CD+ for 100% Log Cue/Silver and CD++ for Gold\n\$f: format\n\$q: quality\n\$l: record label\n\$e: name of the specific edition, if any\n\$n: catalog number\n\$id: identifying info\n\n(Press [enter] for default: \$a (\$y) \$t {\$id} [\$f \$s])\n\nEnter format here (or press [enter])': )" LIBFT
                    break;;
            [nN]* )
                    break;;
            * )
                echo -e "\nPlease answer the question.\n";;
        esac
    done
    
    clearp
    echo -e '\n'${YLW}'>'${NC} 'Music player daemon (mpd) config'
    while true; do
        read -p "$(echo -e "\nDo you wish to configure mpd to allow playing downloaded releases while sorting (y/n)?: ")" MPDCHOICE
        case $MPDCHOICE in
            [yY]* )
                    read -p "$(echo -e "\n${WHT}server:port${NC} information for your MPD server: ")" MPDSERVER
                    read -p "$(echo -e "\nOptional password for controlling the mpd server, as defined in your mpd config: ")" MPDPW
                    read -p "$(echo -e "\nLocal MPD library for your MPD server\n(Note: Varroa will be using symbolic links there to be able to play releases from your download_directory)\n\nmpd library directory (relative paths okay): ")" MPDLIB
                    break;;
            [nN]* )
                    break;;
            * )
                echo -e "\nPlease answer the question.\n";;
        esac
    done
fi
}

config_yaml() {
# general section
echo -e "general:"
    if [ "$MCHOICE" != "2" ]; then
        echo -e "  watch_directory: $GWATCH"
                if [ "$GMETADATA" = "true" ]; then
                    echo -e "  download_directory: $GDOWNLOAD"
                fi
        echo -e "  automatic_metadata_retrieval: $GMETADATA"
    fi
echo -e "  log_level: 1"
echo -e ""

# trackers section
echo -e "trackers:"
echo -e "  - name: red"
echo -e "    user: $TUSER"
echo -e "    password: $TPASS"
echo -e "    url: https://redacted.ch"
echo -e ""

# autosnatch section
if [ "$MCHOICE" != "2" ]; then
    echo -e "autosnatch:"
    echo -e "  - tracker: red"
    echo -e "    irc_server: irc.scratch-network.net:6697"
    echo -e "    irc_key: $ASIRCKEY"
    echo -e "    irc_ssl: true"
    echo -e "    irc_ssl_skip_verify: true"
    echo -e "    nickserv_password: $ASNICKPASS"
    echo -e "    bot_name: $ASBOTNAME"
    echo -e "    announcer: Drone"
    echo -e "    announce_channel: \"#red-announce\""
    echo -e "    blacklisted_uploaders:"
                if [ -n "$BLACKLIST" ]; then
                    echo -e "    - $(echo -e $TUSER,$BLACKLIST | sed 's/,/\n    - /g')"
                else
                    echo -e "    - $(echo -e $TUSER)"
                fi
fi

# stats section
echo -e "stats:"
echo -e "  - tracker: red"
echo -e "    update_period_hour: 1"
    if [ "$MCHOICE" != "2" ]; then
        echo -e "    min_ratio: $STMINRATIO"
        echo -e "    target_ratio: $STTARRATIO"
        echo -e "    max_buffer_decrease_by_period_mb: $STBUFFERDEC"
    fi
echo -e ""
}

# webserver section
webservercfg() {
echo -e "webserver:"
echo -e "  serve_stats: true"
echo -e "  stats_user: $WSUSER"
echo -e "  stats_password: $WSPASS"
echo -e "  allow_downloads: true"
echo -e "  token: $WSTOKEN"
echo -e "  https_port: $WSPORT"
echo -e "  https_hostname: $WSHOST"
echo -e "  theme: dark_green"
echo ""
}

# gitlab stat hosting section
gitlabpage() {
echo -e "gitlab_pages:"
echo -e "  git_https: $GITHTTPS"
echo -e "  user: $GITUSER"
echo -e "  password: $GITPW"
echo ""
}

# notification section
notifications() {
echo -e "notifications:"
echo -e "  pushover:"
echo -e "    token: $NTFYTKN"
echo -e "    user: $NTFYUSER"
echo -e "  webhooks:"
echo -e "    address: $NTFYADDRS"
echo -e "    token: $NTFYWHTKN"
echo -e "    trackers:"
echo -e "    - red"
echo ""
}

# library section
library() {
echo -e "library:"
echo -e "  directory: $LIBDIR"
echo -e "  use_hard_links: $LIBHL"
if [ -n "$LIBFT" ]; then
    echo -e "  folder_template: $LIBFT"
fi
echo ""
}

# mpd section
mpd() {
echo -e "mpd:"
echo -e "  server: $MPDSERVER"
if [ -n "$MPDPW" ]; then
    echo -e "  password: $MPDPW"
fi
echo -e "  library: $MPDLIB"
echo ""
}

# Perfect FLAC CD filter
perfect_flac_CD() {
echo -e "filters:"
echo -e "  - name: $PFNAME"
echo -e "    year:"
echo -e "    - 2017"
echo -e "    source:"
echo -e "    - CD"
echo -e "    format:"
echo -e "    - FLAC"
echo -e "    min_size_mb: 100"
echo -e "    max_size_mb: 1024"
echo -e "    quality:"
echo -e "    - Lossless"
echo -e "    has_cue: true"
echo -e "    has_log: true"
echo -e "    log_score: 100"
echo -e "    allow_duplicates: false"
echo -e ""
}

# additional filters
addlfilters() {
while true
do
    read -p "$(echo -e "\nDo you want to add a filter (or an additional filter) (y/n)? ")" FILTERQ
    case $FILTERQ in
        [yY]* )
                read -p "$(echo -e "\n${CYAN}name${NC}: Name for this filter (use underscores for spaces): ")" FNAME
                
                read -p "$(echo -e "\n${CYAN}watch_directory${NC}: If you want to send torrents of this particular filter to another directory (e.g. another bittorent client's watch folder), then enter it below or leave it blank:\n\b ")" FWATCHD
                
                listnotification
                
                read -p "$(echo -e "\n${CYAN}year${NC}: List of acceptable year(s) (leave blank to allow all): ")" FYEARS
                
                clearf
                
                read -p "$(echo -e "\n${CYAN}perfect_flac${NC}: Shortcut for all settings that define perfect FLACs (overrides ${CYAN}quality${NC}, ${CYAN}source${NC}, ${CYAN}format${NC}, ${CYAN}has_log${NC}, ${CYAN}has_cue${NC}, ${CYAN}log_score${NC} fields)\nEnter \"y\" to enable or leave blank for finer control of above fields: ")" FPFLAC
                    if [ "$FPFLAC" == "y" ]; then
                            FPFLAC=true
                        else
                            FPFLAC=""
                    fi
                
                if [ -z "$FPFLAC" ]; then
                    listnotification
                    read -p "$(echo -e "\n${CYAN}source${NC}: List of acceptable source(s) (leave blank to allow all)\n\n(Available sources: CD,Vinyl,WEB,DVD,SACD,Blu-Ray,Soundboard,DAT,Cassette)\n\nMake your selection: ")" FSOURCE
                    
                    listnotification
                    read -p "$(echo -e "\n${CYAN}format${NC}: List of acceptable audio format(s) (leave blank to allow all)\n\n(Available formats: FLAC,MP3,AAC,DTS,AC3)\n\nMake your selection: ")" FFORMAT
                    
                    listnotification
                    read -p "$(echo -e "\n${CYAN}quality${NC}: List of acceptable audio quality (leave blank to allow all)\n\n(Available encodings: 24bit Lossless,Lossless,320,V0 (VBR),APX (VBR),256,V1 (VBR),V2 (VBR),APS (VBR),192,Other)\n\nMake your selection: ")" FQUALITY
                    
                    clearf
                    
                    read -p "$(echo -e "\n${CYAN}has_cue${NC}: Enter \"y\" (no quotes) if the upload needs to have a cue: ")" FCUE
                        if [ "$FCUE" == "y" ]; then
                            FCUE=true
                        else
                            FCUE=""
                        fi
                    
                    read -p "$(echo -e "\n${CYAN}has_log${NC}: Enter \"y\" (no quotes) if the upload needs to have a log file: ")" FLOG
                        if [ "$FLOG" == "y" ]; then
                            FLOG=true
                        else
                            FLOG=""
                        fi
                        if [ "$FLOG" == "true" ]; then
                            read -p "$(echo -e "\n${CYAN}log_score${NC}: Enter the acceptable minimum log score: ")" FLOGSCORE
                        fi
                fi
                
                listnotification
                read -p "$(echo -e "\n${CYAN}type${NC}: List of acceptable types of release(s) (leave blank to allow all except those excluded later)\n\n(Available release types: Album,EP,Single,Anthology,Compilation,Soundtrack,Live album,Remix,Bootleg,Interview,Mixtape,Demo,Concert Recording,DJ Mix,Unknown)\n\nMake your selection: ")" FTYPE
                
                listnotification
                read -p "$(echo -e "\n${CYAN}excluded_type${NC}: Exclude the following types of release(s) (leave blank to allow all or allow those selected earlier)\n\n(Available release types: Album,EP,Single,Anthology,Compilation,Soundtrack,Live album,Remix,Bootleg,Interview,Mixtape,Demo,Concert Recording,DJ Mix,Unknown)\n\nMake your selection: ")" FXTYPE
                
                listnotification
                read -p "$(echo -e "\n${CYAN}artist${NC}: List of acceptable artist(s), leave blank to accept all artists other than those excluded next (if any)\n\nList included artists (if any): ")" FARTIST
                
                listnotification
                read -p "$(echo -e "\n${CYAN}excluded_artist${NC}: Exclude the following artist(s), leave blank to accept all artists or only those listed previously\n\nList excluded artists (if any): ")" FXARTIST
                
                listnotification
                read -p "$(echo -e "\n${CYAN}record_label${NC}: List of acceptable record label(s) (leave blank to allow all)\n\nList record label(s) (if any): ")" FLABEL
                
                listnotification
                read -p "$(echo -e "\n${CYAN}included_tags${NC}: List of acceptable tag(s) (leave blank to include all tags except those excluded next (if any))\n\nList included tags here (if any): ")" FTAGS
                
                listnotification
                read -p "$(echo -e "\n${CYAN}excluded_tags${NC}: Exclude the following tag(s) (leave blank to exclude all tags except those included previously (if any))\n\nList excluded tags here (if any): ")" FXTAGS
                
                listnotification
                read -p "$(echo -e "\n${CYAN}edition_contains${NC}: List of words or expressions to be found in edition name (e.g. Remaster,Japan,Bonus,Anniversary)\n(leave blank to omit)\n\nMake your selection: ")" FEDITION
                
                clearf
                
                read -p "$(echo -e "\n${CYAN}min_size_mb${NC}: Minimum size for release (in MB) (leave blank for no minimum size): ")" FMIN
                
                read -p "$(echo -e "\n${CYAN}max_size_mb${NC}: Maximum size for release (in MB) (leave blank for no max size): ")" FMAX
                
                listnotification
                read -p "$(echo -e "\n${CYAN}uploader${NC}: Accept releases only from the following uploader(s) (leave blank to allow all)\nMake your selection: ")" FUPLOADER
                
                clearf
                
                read -p "$(echo -e "\n${CYAN}allow_scene${NC}: Enter \"y\" (no quotes) to include scene releases (leave it blank to exclude scene releases): ")" FSCENE
                    if [ "$FSCENE" == "y" ]; then
                        FSCENE=true
                    else
                        FSCENE=""
                    fi
                    
                read -p "$(echo -e "\n${CYAN}allow_duplicates${NC}: Enter \"y\" (no quotes) to allow dupes, leave it blank for the default setting (no dupes): ")" FDUPES
                    if [ "$FDUPES" == "y" ]; then
                        FDUPES=true
                    else
                        FDUPES=""
                    fi
                
                read -p "$(echo -e "\n${CYAN}unique_in_group${NC}: Enter \"y\" (no quotes) to allow autosnatching of torrents if no other torrent in the same group has been snatched before, otherwise leave it blank: ")" FUNIQUE
                    if [ "$FDUPES" == "y" ]; then
                        FUNIQUE=true
                    else
                        FUNIQUE=""
                    fi
                
                echo -e "\nUpdating config.yaml.new..."
                filtersheet >> config.yaml.new
                perl -0777 -pi -e 's/\n\n\n/\n\n/gm' config.yaml.new
                perl -0777 -pi -e 's/filters:\n\n\s{2}-\sname:/filters:\n\  - name:/g' config.yaml.new
                donemsg
                ;;
        [nN]* ) break;;
        * ) echo -e "\nPlease answer the question.\n"
            ;;
    esac
done
}

# Building the filter
filtersheet() {
echo -e ""

echo -e "  - name: $FNAME"

if [ -n "$FWATCHD" ]; then
    echo -e "    watch_directory: $FWATCHD"
fi

if [ -n "$FYEARS" ]; then
    echo -e "    year:"
    echo -e "    - $(echo -e "$FYEARS" | sed 's/,/\n    - /g')"
fi

if [ -n "$FPFLAC" ]; then
    echo -e "    perfect_flac: $FPFLAC"
fi

if [ -n "$FFORMAT" ]; then
    echo -e "    format:"
    echo -e "    - $(echo -e "$FFORMAT" | sed 's/,/\n    - /g')"
fi

if [ -n "$FSOURCE" ]; then
    echo -e "    source:"
    echo -e "    - $(echo -e "$FSOURCE" | sed 's/,/\n    - /g')"
fi

if [ -n "$FTYPE" ]; then
    echo -e "    type:"
    echo -e "    - $(echo -e "$FTYPE" | sed 's/,/\n    - /g')"
fi

if [ -n "$FXTYPE" ]; then
    echo -e "    excluded_type:"
    echo -e "    - $(echo -e "$FXTYPE" | sed 's/,/\n    - /g')"
fi

if [ -n "$FQUALITY" ]; then
    echo -e "    quality:"
    echo -e "    - $(echo -e "$FQUALITY" | sed 's/,/\n    - /g')"
fi

if [ -n "$FCUE" ]; then
    echo -e "    has_cue: $FCUE"
fi

if [ -n "$FLOG" ]; then
    echo -e "    has_log: $FLOG"
fi

if [ -n "$FLOGSCORE" ]; then
    echo -e "    log_score: $FLOGSCORE"
fi

if [ -n "$FARTIST" ]; then
    echo -e "    artist:"
    echo -e "    - $(echo -e "$FARTIST" | sed 's/,/\n    - /g')"
fi

if [ -n "$FXARTIST" ]; then
    echo -e "    excluded_artist:"
    echo -e "    - $(echo -e "$FXARTIST" | sed 's/,/\n    - /g')"
fi

if [ -n "$FLABEL" ]; then
    echo -e "    record_label":
    echo -e "    - $(echo -e "$FLABEL" | sed 's/,/\n    - /g')"
fi

if [ -n "$FTAGS" ]; then
    echo -e "    included_tags:"
    echo -e "    - $(echo -e "$FTAGS" | sed 's/,/\n    - /g')"
fi

if [ -n "$FXTAGS" ]; then
    echo -e "    excluded_tags:"
    echo -e "    - $(echo -e "$FXTAGS" | sed 's/,/\n    - /g')"
fi

if [ -n "$FSCENE" ]; then
    echo -e "    allow_scene: $FSCENE"
fi

if [ -n "$FEDITION" ]; then
    echo -e "    edition_contains:"
    echo -e "    - $(echo -e "$FEDITION" | sed 's/,/\n    - /g')"
fi

if [ -n "$FUPLOADER" ]; then
    echo -e "    uploader:"
    echo -e "    - $(echo -e "$FUPLOADER" | sed 's/,/\n    - /g')"
fi

if [ -n "$FMIN" ]; then
    echo -e "    min_size_mb: $FMIN"
fi

if [ -n "$FMAX" ]; then
    echo -e "    max_size_mb: $FMAX"
fi

if [ -n "$FDUPES" ]; then
    echo -e "    allow_duplicates: $FDUPES"
fi

if [ -n "$FUNIQUE" ]; then
    echo -e "    unique_in_group: $FUNIQUE"
fi
echo -e ""
}

# Entry Confirmation
clearp() {
read -p "$(echo -e '\nPress [ENTER] to continue...')"
clear
}

listnotification() {
clear
echo -e "${WHT}Filter${NC}: $FNAME"
echo -e "\n${YLW}NOTE${NC}: Separate items by commas (no spaces between items): e.g. foo1,foo2,foo3"
}

clearf() {
clear
echo -e "${WHT}Filter${NC}: $FNAME"
}

donemsg() {
echo -e "\n[${RED}Done${NC}]"
}

main() {
clear
#echo -e ">>> Varroa Musica config.yaml Manager <<<\n"
while true
do
#    echo -e "┌$(for each in $(seq 1 50); do printf "─"; done)···"
    echo -e "\n┌┤ Varroa Musica config.yaml Manager ├─···"
    echo -e "││      for Varroa Musica v18        │"
    echo -e "│  "
    echo -e "│  Choose one of the following:"
    echo -e "│  "
    echo -e "│  [1] Generate a new config.yaml"
    echo -e "│  [2] Generate a new config.yaml (stats only)"
    echo -e "│  [3] Add a filter"
    echo -e "│  [4] Remove a filter"
    echo -e "│  "
    echo -e '│  (Press ^C at any time to exit the script)'
    echo -e '│  '
    read -p "$(echo -e "└─: ")" MCHOICE
    
    case $MCHOICE in
        1 )
            # Config.yaml for snatching and stats
            clear
            q_and_a
            echo -e "\nGenerating config.yaml.new..."
            config_yaml > config.yaml.new
            if [ "$WSCHOICE" == "y" ]; then
                webservercfg >> config.yaml.new
            fi
            if [ "$GITLCHOICE" == "y" ]; then
                gitlabpage >> config.yaml.new
            fi
            if [ "$NTFYCHOICE" == "y" ]; then
                notifications >> config.yaml.new
            fi
            if [ "$LIBCHOICE" == "y" ]; then
                library >> config.yaml.new
            fi
            if [ "$MPDCHOICE" == "y" ]; then
                mpd >> config.yaml.new
            fi
            donemsg
            clearp
            echo -e "${YLW}NOTE${NC}: A perfect flac filter for 2017 CD's with 100% log+cue can be included.\nMinimum size of the release 100MB and the maximum size 1GB.\n"
            while true
            do
                read -p "Do you want to include this filter (y/n)? " PFCHOICE
                case $PFCHOICE in
                    [yY]* )
                            read -p "$(echo -e "\nPlease provide a name for this filter"): " PFNAME
                            echo -e "\nUpdating config.yaml.new with this filter."
                            perfect_flac_CD >> config.yaml.new
                            break;;
                    [nN]* )
                            echo -e "filters:" >> config.yaml.new
                            break;;
                    * ) echo -e "\nPlease answer the question.\n";;
                esac
            done
            clearp
            addlfilters
            break;;
        2 ) 
            # Config.yaml for stats only
            clear
            q_and_a
            echo -e "\nGenerating config.yaml.new for stats reporting..."
            config_yaml > config.yaml.new
            webservercfg >> config.yaml.new
            if [ "$GITLCHOICE" == "y" ]; then
                gitlabpage >> config.yaml.new
            fi
            donemsg
            clearp
            break;;
        3 )
            # Adding a new filter
            echo -e "\nCopying your existing config.yaml to config.yaml.new:" 
            cp -v ./config.yaml config.yaml.new
            addlfilters
            break;;
        4 )
            # Removing filter(s)
            echo -e "\nCopying your existing config.yaml to config.yaml.new:" 
            cp -v ./config.yaml config.yaml.new
            echo -e "\nList of current filters: "
            echo -e "\n$(grep '^  - name: ' config.yaml.new | sed 's/^  - name: //g' | awk '{if (NR!=1) {print}}')"
            read -p "$(echo -e "\nWhich filter to remove? ")" RMFILTER
            perl -0777 -pi -e "s/(\s{2}-\sname:\s)$RMFILTER(?:\n.*?){1,}\1/\1/gm" config.yaml.new
            perl -0777 -pi -e "s/(\s{2}-\sname:\s)$RMFILTER(?:\n.*?){1,}$//gm" config.yaml.new
            donemsg
            
            while true
            do
                read -p "$(echo -e "\nDo you want to remove another filter? ")" RMFANOTHER
                    case $RMFANOTHER in
                        [yY]* )
                                echo -e "\nList of current filters: "
                                echo -e "\n$(grep '^  - name: ' config.yaml.new | sed 's/^  - name: //g' | awk '{if (NR!=1) {print}}')"
                                read -p "$(echo -e "\nWhich filter to remove? ")" RMFILTER
                                perl -0777 -pi -e "s/(\s{2}-\sname:\s)$RMFILTER(?:\n.*?){1,}\1/\1/gm" config.yaml.new
                                perl -0777 -pi -e "s/(\s{2}-\sname:\s)$RMFILTER(?:\n.*?){1,}$//gm" config.yaml.new
                                donemsg
                                ;;
                        [nN]* )
                                break;;
                        * )
                                ;;
                    esac
            done
            break;;
        * )
            echo -e "\nPlease make an appropriate selection.\n"
            ;;
    esac
done
echo -e '\nCheck your generated (or edited) config.yaml.new, \ne.g. cat ./config.yaml.new | more\n\nIf satisfied, change into the directory of the varroa binary, (if needed) stop the running varroa process (varroa stop) and replace config.yaml with config.yaml.new, \ne.g. mv config.yaml.new config.yaml'
}

main
